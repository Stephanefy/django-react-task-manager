import { lightGreen, cyan, amber, red } from "@mui/material/colors";

const difficultyOptionData = {
    1: {
        label: "Débutant",
        color: lightGreen[500],
    },
    2: {
        label: "Intermédiaire",
        color: cyan[500],
    },
    3: {
        label: "Avancé",
        color: amber[500],
    },
};
export const difficultyOptionDataList = Object
    .keys(difficultyOptionData)
    .map(key => ({ key, ...difficultyOptionData[key], value: key }));

export default difficultyOptionData;