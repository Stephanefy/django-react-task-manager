import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import {
    Box,
    TextField,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    FormHelperText,
    Grid,
    Paper,
    Typography,
    Button
} from "@mui/material";

import { Link, useNavigate, useParams } from "react-router-dom";
import * as yup from "yup";
import useRequestResource from "src/hooks/useRequestResource";

import ColorBox from "src/components/ColorBox";
import difficultyOptionData, { difficultyOptionDataList } from "src/data/difficultyOptionData";


const validationSchema = yup.object({
    name: yup.string().required("le nom de l'exercice est requis").max(100, "Max length is 100"),
    description: yup.string(),
    category: yup.number().required("la catégorie est requise"),
    difficulty: yup.string().required("Le choix de la difficulté est requis"),
});



export default function ExerciceDetails() {
    const { getResourceList, resourceList: categoryList } = useRequestResource({ endpoint: "categories" });
    const { addResource, updateResource, getResource, resource } = useRequestResource({ endpoint: "exercise", resourceLabel: "Exercise" })
    const navigate = useNavigate();
    const { id } = useParams();
    const [initialValues, setInitialValues] = useState({
        name: "",
        description: "",
        category: "",
        difficulty: 2,
    });

    useEffect(() => {
        getResourceList();
    }, [getResourceList])

    useEffect(() => {
        if (id) {
            getResource(id);
        }
    }, [id, getResource])

    useEffect(() => {
        if (resource) {
            setInitialValues({
                title: resource.name,
                description: resource.description || "",
                category: resource.category,
                difficulty: resource.difficulty
            })
        }
    }, [resource])

    const handleSubmit = (values) => {


        console.log(values)

        if (id) {
            updateResource(id, values, () => {
                navigate("/tasks")
            })
            return;
        }
        addResource(values, () => {
            navigate("/tasks")
        }, true)
    };

    return (
        <Paper sx={{
            borderRadius: (theme) => theme.spacing(0.5),
            boxShadow: (theme) => theme.shadows[5],
            padding: (theme) => theme.spacing(3)
        }}>
            <Typography variant="h6" mb={4}>
                {id ? "Modifier l'exercice" : "Create un exercice"}
            </Typography>
            <Formik
                onSubmit={handleSubmit}
                initialValues={initialValues}
                enableReinitialize
                validationSchema={validationSchema}
            >
                {(formik) => {
                    return (
                        <form onSubmit={formik.handleSubmit}>
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="name"
                                        label="Nom de l'exercice"
                                        {...formik.getFieldProps("name")}
                                        error={formik.touched.name && Boolean(formik.errors.name)}
                                        helperText={formik.touched.name && formik.errors.name}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        multiline
                                        minRows={4}
                                        id="description"
                                        label="Description"
                                        {...formik.getFieldProps("description")}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl
                                        sx={{

                                            width: "100%",
                                        }}
                                        error={
                                            formik.touched.difficulty && Boolean(formik.errors.difficulty)
                                        }
                                    >
                                        <InputLabel id="priority-label">Difficulté</InputLabel>
                                        <Select
                                            fullWidth
                                            labelId="difficulty-label"
                                            label="Difficulty"
                                            id="difficulty"
                                            {...formik.getFieldProps("difficulty")}
                                        >
                                            {Array.isArray(difficultyOptionDataList)
                                                ? difficultyOptionDataList.map((p) => {
                                                    return (
                                                        <MenuItem value={p.value} key={p.value}>
                                                            <Box
                                                                sx={{
                                                                    display: "flex",
                                                                    alignItems: "center",
                                                                }}
                                                            >
                                                                <ColorBox
                                                                    color={
                                                                        difficultyOptionData[p.value].color ||
                                                                        "#fff"
                                                                    }
                                                                />
                                                                <Box sx={{ ml: 1 }}>{p.label}</Box>
                                                            </Box>
                                                        </MenuItem>
                                                    );
                                                })
                                                : null}
                                        </Select>
                                        <FormHelperText>
                                            {formik.touched.difficulty && formik.errors.difficulty}
                                        </FormHelperText>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl
                                        sx={{

                                            width: "100%",
                                        }}
                                        error={
                                            formik.touched.category && Boolean(formik.errors.category)
                                        }
                                    >
                                        <InputLabel id="category-label">Catégorie</InputLabel>
                                        <Select
                                            fullWidth
                                            labelId="category-label"
                                            label="Category"
                                            id="category"
                                            {...formik.getFieldProps("category")}
                                        >
                                            {Array.isArray(categoryList.results)
                                                ? categoryList.results.map((c) => {
                                                    return (
                                                        <MenuItem value={c.id} key={c.id}>
                                                            <Box
                                                                sx={{
                                                                    display: "flex",
                                                                    alignItems: "center",
                                                                }}
                                                            >
                                                                <ColorBox color={`#${c.color}`} />
                                                                <Box sx={{ ml: 1 }}>{c.name}</Box>
                                                            </Box>
                                                        </MenuItem>
                                                    );
                                                })
                                                : null}
                                        </Select>
                                        <FormHelperText>
                                            {formik.touched.category && formik.errors.category}
                                        </FormHelperText>
                                    </FormControl>
                                </Grid>
                                <Grid item>
                                    <Box sx={{ display: "flex", margin: (theme) => theme.spacing(1), marginTop: (theme) => theme.spacing(3) }}>
                                        <Button
                                            component={Link}
                                            to='/tasks'
                                            size="medium"
                                            variant="outlined"
                                            sx={{ mr: 2 }}>
                                            Back
                                        </Button>

                                        <Button type="submit"
                                            size="medium"
                                            variant="contained"
                                            color="primary"
                                        >
                                            Submit
                                        </Button>
                                    </Box>
                                </Grid>
                            </Grid>
                        </form>
                    );
                }}
            </Formik>
        </Paper>
    );
}
