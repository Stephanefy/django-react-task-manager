import React, { useState, useMemo, useEffect } from "react";
import { Calendar, dateFnsLocalizer, Views } from "react-big-calendar";
import format from "date-fns/format";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import getDay from "date-fns/getDay";
import fr from "date-fns/esm/locale/fr";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { Button, Box } from "@mui/material";
import { Link, useLocation, useNavigate } from "react-router-dom";
import useRequestResource  from '../../../hooks/useRequestResource'
import ExercicesListItem from "../Exercises/ExercicesListItem";

const locales = {
  fr: fr,
};
const lang = {
  fr: {
    week: "La semaine",
    work_week: "Semaine de travail",
    day: "Jour",
    month: "Mois",
    previous: "Antérieur",
    next: "Prochain",
    today: `Aujourd'hui`,
    agenda: "Ordre du jour",

    showMore: (total) => `+${total} plus`,
  },
};
const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

const ColoredDateCellWrapper = ({ children }) =>
  React.cloneElement(React.Children.only(children), {
    style: {
      backgroundColor: "lightblue",
    },
  });

const WCalendar = () => {

  const location = useLocation();

  const { resourceList, getResourceList, deleteResource, updateResource } = useRequestResource({ endpoint: "workout", resourceLabel: "Workout" });

  const { components, defaultDate, messages, max, views } = useMemo(
    () => ({
      components: {
        timeSlotWrapper: ColoredDateCellWrapper,
      },
      messages: lang["fr"],
      defaultDate: Date.now(),
      views: Object.keys(Views).map((k) => Views[k]),
    }),
    []
  );

    console.log(resourceList)




  useEffect(() => {
    getResourceList({},false,true)
    // setEvents(prevState => ))
    }, [getResourceList])

//   useEffect(() => {
//     getResourceList({ query: location.search });
// }, [getResourceList, location.search])

const events = resourceList.results.map(session => {
      return {
        title: session.title,
        start: new Date(session.start_at),
        end: new Date(session.end_at)
      }
    })
console.log('events object', events)


  return (
    <div>
      <Box sx={{ display: "flex", justifyContent: "flex-end", mb: 3, mt: 3 }}>
        <Button
          component={Link}
          variant="contained"
          color="primary"
          to="/workout/create"
        >
          Créer un session
        </Button>
      </Box>
      <Box sx={{ display: "flex", justifyContent: "flex-end", mb: 3, mt: 3 }}>
        <Button
          component={Link}
          variant="contained"
          color="primary"
          to="/workout/exercices/create"
        >
          Enregistrer un type d'exercice
        </Button>
      </Box>
      <Calendar
        localizer={localizer}
        startAccessor="start"
        endAccessor="end"
        culture="fr"
        showMultiDayTimes
        defaultDate={defaultDate}
        views={views}
        step={60}
        messages={messages}
        events={events}
      />
    </div>
  );
};

export default WCalendar;
