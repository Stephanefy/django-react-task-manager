import React, { useEffect, useState } from "react";
import { Formik, setIn } from "formik";
import AddBoxIcon from "@mui/icons-material/AddBox";
import {
  Box,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
  IconButton,
  Grid,
  Paper,
  Typography,
  Button,
} from "@mui/material"

// TO DO request Workout session data and display it on calendar

import SelectInput from "src/components/SelectInput";

import { Link, useNavigate, useParams } from "react-router-dom";
import * as yup from "yup";
import useRequestResource from "src/hooks/useRequestResource";

import ColorBox from "src/components/ColorBox";
import priorityOptionsData, {
  priorityOptionsDataList,
} from "src/data/priorityOptionsData";
import { typography } from "@mui/system";

const validationSchema = yup.object({
  title: yup
    .string()
    .required("Title is required")
    .max(100, "Max length is 100"),
  description: yup.string().notRequired(),
});

export default function WorkoutDetails() {
  const { getResourceList, resourceList: exerciseList } = useRequestResource({
    endpoint: "exercise",
  });
  const { addResource, updateResource, getResource, resource } =
    useRequestResource({
      endpoint: "workout",
      resourceLabel: "Workout session",
    });
  const [selectExercices, setSelectedExercise] = useState([{id: Math.floor(Math.random() * 100), selectedValue:""}]);
  const [exercices, setExercices] = useState([]);


  const [initialValues, setInitialValues] = useState({
    title: "",
    description: "",
    start_at: new Date(),
    end_at: new Date()
  });

//   const [countExercices, setCountExercices] = useState({
//     count: 1,
//     exercises: function () {
//       return Array.from({ length: this.count }, () => ({}));
//     },
//   });

  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    getResourceList(true);
  }, [getResourceList]);

  useEffect(() => {
    if (id) {
      getResource(id);
    }
  }, [id, getResource]);

  useEffect(() => {
    if (resource) {
      setInitialValues({
        title: resource.title,
        description: resource.description || "",
        category: resource.category,
        priority: resource.priority,
      });
    }
  }, [resource]);

  const handleDelete = (itemId) => {
    setSelectedExercise(selectExercices.filter(ex => ex.id !== itemId))
  }

  const handleSubmit = (values) => {
    console.log("reached");
    console.log(selectExercices);
    console.log(exercices)

    const data = {
      ...values,
      exercices: [...exercices],
    };

    console.log('final data', data)
    addResource(data);

    setExercices([])

    // if (id) {
    //     updateResource(id, values, () => {
    //         navigate("/tasks")
    //     })
    //     return;
    // }
    // addResource(values, () => {
    //     navigate("/tasks")
    // })
  };

  useEffect(() => {
    if(selectExercices.length === 0) {
      setSelectedExercise([{id: Math.floor(Math.random() * 100), selectedValue:""}])
    }
  }, [selectExercices])

  return (
    <Paper
      sx={{
        borderRadius: (theme) => theme.spacing(0.5),
        boxShadow: (theme) => theme.shadows[5],
        padding: (theme) => theme.spacing(3),
      }}
    >
      <Typography variant="h6" mb={4}>
        {id ? "Modifier la session" : "Créer une session"}
      </Typography>
      <Formik
        onSubmit={handleSubmit}
        initialValues={initialValues}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    id="title"
                    label="Title"
                    {...formik.getFieldProps("title")}
                    error={formik.touched.title && Boolean(formik.errors.title)}
                    helperText={formik.touched.title && formik.errors.title}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    multiline
                    minRows={4}
                    id="description"
                    label="Description"
                    {...formik.getFieldProps("description")}
                  />
                </Grid>
                <Grid item xs={12}>
                  <IconButton
                                            aria-label="ajouter-exercice"
                                            color="primary"
                                            onClick={() =>
                                              setSelectedExercise((prevState) => [...prevState, {id: Math.floor(Math.random() * 100), selectedValue:""}])
                                            }
                                            sx={{
                                              width:'100%',
                                              textAlign: 'right'
                                            }}
                                          >
                                            <Typography>Ajouter un exercice à votre session</Typography>
                                            <AddBoxIcon />
                  </IconButton>
                  <ul>
                    {selectExercices?.map((item, idx) => (
                        <SelectInput 
                          formik={formik} 
                          exerciseList={exerciseList} 
                          item={item}
                          handleDelete={handleDelete}
                          setExercices={setExercices}
                          selectExercices={selectExercices}
                          setSelectedExercise={setSelectedExercise}
                          />
                    ))}
                  </ul>

                </Grid>
                <Grid item xs={12}>
                  <TextField
                      fullWidth
                      minRows={4}
                      id="start_at"
                      label="date et heure de début de la session"
                      type="datetime-local"
                      InputLabelProps={{
                        shrink: true
                      }}
                      {...formik.getFieldProps("start_at")}
                    />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                      fullWidth
                      minRows={4}
                      id="start_at"
                      label="date et heure de fin de la session"
                      type="datetime-local"
                      InputLabelProps={{
                        shrink: true
                      }}
                      {...formik.getFieldProps("end_at")}
                    />
                </Grid>

                <Grid item>
                  <Box
                    sx={{
                      display: "flex",
                      margin: (theme) => theme.spacing(1),
                      marginTop: (theme) => theme.spacing(3),
                    }}
                  >
                    <Button
                      component={Link}
                      to="/tasks"
                      size="medium"
                      variant="outlined"
                      sx={{ mr: 2 }}
                    >
                      Retour
                    </Button>

                    <Button
                      type="submit"
                      size="medium"
                      variant="contained"
                      color="primary"
                    >
                      Valider
                    </Button>
                  </Box>
                </Grid>
              </Grid>
            </form>
          );
        }}
      </Formik>
    </Paper>
  );
}
