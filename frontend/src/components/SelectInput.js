import React from 'react'
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import {
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    FormHelperText,
    Box
} from "@mui/material";


import ColorBox from "src/components/ColorBox";
import priorityOptionsData, {
  priorityOptionsDataList,
} from "src/data/priorityOptionsData";

const SelectInput = ({formik, exerciseList, item, setExercices, setSelectedExercise, handleDelete}) => {

  console.log('fdsfsd', exerciseList)


  return (
    <>
                        <FormControl
                          sx={{
                            width: "100%",
                          }}
                          error={
                            formik.touched.category &&
                            Boolean(formik.errors.category)
                          }
                          id={item.id}
                        >
                      <li style={{listStyleType: "none"}} 
                      
                          id={item.id}>
                          <InputLabel id="category-label">Exercices</InputLabel>
                          <Select
                            fullWidth
                            labelId="exercice-label"
                            label="Exercices"
                            id="exercises"
                            // {...formik.getFieldProps("exercises")}
                            onChange={(e) =>
                              setExercices((prevState) => [
                                ...prevState,
                                e.target.value,
                              ])
                            }
                          >
                            { exerciseList.results
                              ? exerciseList.results.map((e) => {
                                  return (
                                    <MenuItem
                                      name={e.id}
                                      value={e.name}
                                      key={e.id}
                                    >
                                      <Box
                                        sx={{
                                          display: "flex",
                                          alignItems: "center",
                                        }}
                                      >
                                        <ColorBox color={`#${e.color}`} />
                                        <Box sx={{ ml: 1 }}>{e.name}</Box>
                                      </Box>
                                    </MenuItem>
                                  );
                                })
                              : null}
                          </Select>
                          <FormHelperText>
                            {formik.touched.category && formik.errors.category}
                          </FormHelperText>
                      </li>
                        </FormControl>
                                          <Box
                                          sx={{
                                            display: "flex",
                                            width: "100%",
                                            justifyContent: "end",
                                            marginTop:'0.1em'
                                          }}
                                        >

                                          <IconButton
                                            aria-label="supprimer-exercice"
                                            color="secondary"
                                            onClick={() => handleDelete(item.id)}
                                          >
                                            <DeleteIcon />
                                          </IconButton>
                                        </Box>
                      </>
  )
}

export default SelectInput