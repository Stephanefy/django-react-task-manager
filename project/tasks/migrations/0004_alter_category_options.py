# Generated by Django 4.0.2 on 2022-04-04 14:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0003_task'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name_plural': 'categories'},
        ),
    ]
