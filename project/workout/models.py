from enum import unique
from django.db import models
from django.contrib.auth import get_user_model

from tasks.models import Category

User = get_user_model()






class Exercices(models.Model):

    class Difficulty(models.IntegerChoices):
        BEGINNER = 1, "Beginner"
        INTERMEDIATE = 2, "Intermediate"
        ADVANCED = 3, "Advanced"


    name = models.CharField(max_length=200, blank=False)
    category = models.ForeignKey(
        Category, related_name="exercices", on_delete=models.CASCADE)
    description = models.TextField(max_length=500, blank=True, null=True)
    difficulty = models.PositiveIntegerField(choices=Difficulty.choices, default=Difficulty.BEGINNER )
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, related_name="exercices", on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.name)

class WorkoutSession(models.Model):
    title = models.CharField(max_length=250, blank=False, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    start_at = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True)
    end_at = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="workout_session", blank=True, null=True)


class WorkoutSessionExercises(models.Model):
    exercise = models.ForeignKey('Exercices', on_delete=models.CASCADE, related_name="session_exercice")
    workout_session = models.ForeignKey('WorkoutSession', on_delete=models.CASCADE, related_name="exercises")



