from rest_framework import serializers
from .models import Exercices, WorkoutSession, WorkoutSessionExercises


class ExerciseSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(
        read_only=True, source='category.name'
    )
    category_color = serializers.CharField(
        read_only=True, source='category.color'
    )

    class Meta:
        model = Exercices
        fields = ['name','description','category_id','difficulty', 'category_name', 'category_color']
        read_only_fields = ['created_by']


class WorkoutSessionExercisesSerializer(serializers.ModelSerializer):
    exercise = serializers.StringRelatedField(read_only=True)

    class Meta:
            model = WorkoutSessionExercises
            fields = ['id','exercise']

class WorkoutSessionSerializer(serializers.ModelSerializer):
    
    exercises = WorkoutSessionExercisesSerializer(many=True, read_only=True)


    class Meta:
        model = WorkoutSession
        fields = ['id', 'title', 'description', 'start_at', 'created_at','end_at', 'created_by', 'exercises']


