from django.shortcuts import render

from .models import WorkoutSession, WorkoutSessionExercises, Exercices

from .permissions import ExercicePermission
from .serializers import Exercices, ExerciseSerializer, WorkoutSessionSerializer
from rest_framework import viewsets, permissions, filters
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination




class StandardResultSetPagination(PageNumberPagination):
    page_size = 6
    page_size_query_param = 'page_size'
    max_page_size = 6


class ExercicesViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
        ExercicePermission
    ]
    serializer_class = ExerciseSerializer
    pagination_class = StandardResultSetPagination
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['name']
    ordering_fields = ['-created_at']
    ordering = ['-created_at']

    def get_queryset(self):
        user = self.request.user
        difficulty = self.request.query_params.get('difficulty')
        category = self.request.query_params.get('category')
        query_params = {}


        if difficulty is not None:
            query_params["difficulty"] = difficulty

        if category is not None:
            query_params["category"] = category

        return Exercices.objects.filter(created_by=user, **query_params)

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class WorkoutSessionViewSet(viewsets.ModelViewSet):


    permission_classes = [
        permissions.IsAuthenticated,
    ]


    serializer_class = WorkoutSessionSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['name']
    ordering_fields = ['-created_at']
    ordering = ['-created_at']

    def get_queryset(self):
        
        user = self.request.user
        start_at = self.request.query_params.get('start_at')
        end_at = self.request.query_params.get('end_at')
        query_params = {}


        if start_at is not None:
            query_params["difficulty"] = start_at

        if end_at is not None:
            query_params["category"] = end_at

        return WorkoutSession.objects.filter(created_by=user, **query_params)

    
    def perform_create(self, serializer):
        print(self.request.data)
        
        exercises = self.request.data['exercices']
        del self.request.data['exercices']

        print('workout session', self.request.data)
        print('test',exercises)        
        serializer.save(created_by=self.request.user)
    
        for exercise in exercises:
            WorkoutSessionExercises.objects.create(exercise=Exercices.objects.get(name=exercise), workout_session= WorkoutSession.objects.get(title=self.request.data['title'])
            )

        
