from rest_framework import routers
from .views import ExercicesViewSet, WorkoutSessionViewSet

router = routers.DefaultRouter()
router.register(r'api/exercise', ExercicesViewSet, 'exercices')
router.register(r'api/workout', WorkoutSessionViewSet, 'workout-session')


urlpatterns = router.urls
